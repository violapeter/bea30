const textEl = document.querySelector('.ribbon');
const actionEl = document.querySelector('.ribbon-container');
let confettiStart = () => {};

(function setupConfetti() {
  if (!confetti) return;

  confetti.gradient = true;
  confetti.alpha = 0.7;
  confettiStart = () => confetti.start(1200);
})();

function getRandomInt(min, max = Math.MAX_SAFE_INTEGER) {
  return min + Math.floor(Math.random() * (max - min + 1));
}

function selectRandomFromArray(array) {
  return array[getRandomInt(0, array.length - 1)];
}

function getARandomText() {
  return selectRandomFromArray(data);
}

function setRandomText(el) {
  let next = getARandomText();
  const current = el.innerHTML;

  while (next === current) {
    next = getARandomText();
  }

  el.innerHTML = next;
}

function hurray() {
  setRandomText(textEl);
  confettiStart();
}

actionEl.addEventListener('click', hurray);

hurray();
