const data = [
  'Megtanultam latinul',
  'Perfektül megtanultam németül',
  'Megtanultam angolul',
  'Túléltem a gyerekkort a nagyobbik öcsémmel',
  'Megnyertem egy országos irodalmi pályázatot',
  'Egy országos online híroldal szerkesztője voltam',
  'Au pair-kedtem Németországban',
  'Évekig tanítottam mindenféle iskolában',
  'Blogot indítottam',
  'Találtam egy irtó klassz srácot Happen-en',
  'Végigalbérleteztem Budapestet, legalább 3-szor',
  'Megmásztam a Krinnenspitzét',
  'Háromszor körbebicikliztem a Balatont',
  'Elstoppoltam Lengyelországig',
  'Megjelent novellám a Vigíliában',
  'Couchsurföltem Ausztriában',
  'Megtanultam kötni',
  'Bejártam Európát, Vilniustól Krujáig',
  'Láttam a Vatikánt',
  'Megtanultam varrógéppel varrni',
  'Tanultam franciául is!',
  'Elolvastam nagyon sok jó könyvet',
  'Levezettem egy antiochiás nagyhétvégét',
  'Megírtam két szakdolgozatot',
  'Túléltem egy csomó cserkésztábort',
  'Dudált nekem egy TGV Franciaországban',
  'Elvégeztem egy kreatív írás tanfolyamot',
  'Nyelvjárást gyűjtöttem Szlovákiában',
  'Jártam az Északi-tengernél',
  'Táncoltam hajnalig az ignácos bulikban',
  'Felszedtem egy-két spanyol szót Barcelonában',
  'Nyaraltam az Adrián',
  'Partiztam a Bokros Ferenc-emlékteraszon',
  'Éjszakába nyúlóan kentkupéztam Ordacsehiben',
  'Megtanultam Forró de Domingózni',
  'Megrendeztem A király házasságát',
  'Pingpongoztam Lellén Anyával',
  'Őrületeset buliztam a CsakNekedKislány-koncerteken',
  'Pisiltem a Forum Romanumon',
  'Gondviselés-túráztam a Balatonnál',
  'OKTV középdöntős voltam latinból',
  'Megjelentek verseim a Somogyban',
  'Interjút készítettem Csíkszentmihályi Mihállyal',
  'Tudósítottam az Íróakadémia első táboráról',
  'Megnyertem a Tihanyi Babamúzeum mesepályázatát',
  'Kisvonat hozta ki a sörömet Prágában',
  'Hullócsillagokat néztem a Balaton-parton',
  'Jártam a Champs-Élysées-n',
  '“Valöriiiiiiii, szeretlek!!!”',
  'A szakkoliban színvonalas szimpóziumokat szerveztem',
  'Kidobtam Valért az ablakon',
  'Az első sorban csápoltam Lovasinak',
  'Visszaszereztem a kauciómat',
  'Elzarándokoltam Palkonyáig, gyalog',
  'Kibírtam 6 hetet Rügen szigetén',
  'Szánkóztam egy berni pásztorral',
  'Fagyiztam Assisiben',
  'Bicikliztem a sváb nyugdíjasklubbal Ehningenben',
  'Elnyertem a Kaslik-ösztöndíjat',
];
